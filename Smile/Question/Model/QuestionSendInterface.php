<?php
/**
 * Smile Question.
 * @author    Oksana Borodina <b_oksa@ukr.net>
 * Copyright (c) 2020.
 */

declare(strict_types=1);

namespace Smile\Question\Model;

interface QuestionSendInterface
{
    /**
     * Email Template
     */
    const EMAIL_TEMPLATE = 'question_send_mail_template';

    /**
     * General Name
     */
    const EMAIL_GENERAL_NAME = 'trans_email/ident_general/name';

    /**
     * General Email
     */
    const EMAIL_GENERAL_EMAIL = 'trans_email/ident_general/email';

    /**
     * Send answer
     *
     * @param array $vars
     * @return mixed
     */
    public function sendEmail(array $vars);
}
