<?php
/**
 * Smile Question.
 * @author    Oksana Borodina <b_oksa@ukr.net>
 * Copyright (c) 2020.
 */

declare(strict_types=1);

namespace Smile\Question\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Smile\Question\Api\Data\QuestionInterface;

/**
 * Class Question
 * @package Smile\Question\Model
 */
class Question extends AbstractModel implements QuestionInterface, IdentityInterface
{

    /**
     * cache tag.
     */
    const CACHE_TAG = 'email_contact_us';

    /**
     * Initialize resource model
     * @return void
     */
    public function _construct()
    {
        $this->_init('Smile\Question\Model\ResourceModel\Question');
    }

    /**
     * Get identities.
     *
     * @return array
     */
    public function getIdentities(): array
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->getData(self::NAME);
    }

    /**
     * Set Name.
     *
     * @param string $name
     *
     * @return QuestionInterface
     */
    public function setName(string $name): QuestionInterface
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get Email.
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * Set Email.
     *
     * @param string $email
     *
     * @return QuestionInterface
     */
    public function setEmail(string $email): QuestionInterface
    {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * Get Telephone.
     *
     * @return string
     */
    public function getTelephone(): string
    {
        return $this->getData(self::TELEPHONE);
    }

    /**
     * Set Telephone.
     *
     * @param string $telephone
     *
     * @return QuestionInterface
     */
    public function setTelephone(string $telephone): QuestionInterface
    {
        return $this->setData(self::TELEPHONE, $telephone);
    }

    /**
     * Get Question.
     *
     * @return string
     */
    public function getComment(): string
    {
        return $this->getData(self::COMMENT);
    }

    /**
     * Set Comment
     *
     * @param string $comment
     *
     * @return QuestionInterface
     */
    public function setComment(string $comment): QuestionInterface
    {
        return $this->setData(self::COMMENT, $comment);
    }

    /**
     * Get Answer.
     *
     * @return string|null
     */
    public function getAnswer(): ?string
    {
        return $this->getData(self::ANSWER);
    }

    /**
     * Set Answer
     *
     * @param string $answer
     *
     * @return QuestionInterface
     */
    public function setAnswer(string $answer): QuestionInterface
    {
        return $this->setData(self::ANSWER, $answer);
    }

    /**
     * Get Status.
     *
     * @return int
     */
    public function getStatus(): int
    {
        return (int) $this->getData(self::STATUS);
    }

    /**
     * Get Statuses
     *
     * @return array
     */
    public function getStatuses(): array
    {
        return [self::STATUS_NEW => __('New'), self::STATUS_ANSWERED => __('Answered')];
    }

    /**
     * Set Status.
     *
     * @param int $status
     *
     * @return QuestionInterface
     */
    public function setStatus(int $status): QuestionInterface
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get creation time.
     *
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set creation time.
     *
     * @param string $creationTime
     *
     * @return QuestionInterface
     */
    public function setCreatedAt(string $creationTime): QuestionInterface
    {
        return $this->setData(self::CREATED_AT, $creationTime);
    }

    /**
     * Get updated time.
     *
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * Set updated time.
     *
     * @param string $updatedTime
     *
     * @return QuestionInterface
     */
    public function setUpdatedAt(string $updatedTime): QuestionInterface
    {
        return $this->setData(self::UPDATED_AT, $updatedTime);
    }

}
