<?php
/**
 * Smile Question.
 * @author    Oksana Borodina <b_oksa@ukr.net>
 * Copyright (c) 2020.
 */

declare(strict_types=1);

namespace Smile\Question\Model;

use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Smile\Question\Api\Data\QuestionInterface;
use Smile\Question\Api\QuestionRepositoryInterface;
use Smile\Question\Model\QuestionFactory as QuestionFactory;
use Smile\Question\Model\ResourceModel\Question as ResourceQuestion;

/**
 * Class QuestionRepository
 *
 * @package Smile\Question\Model
 */
class QuestionRepository implements QuestionRepositoryInterface
{
    /**
     * Resource question.
     *
     * @var ResourceQuestion
     */
    private $resource;

    /**
     * Question factory.
     *
     * @var QuestionFactory
     */
    private $questionFactory;

    /**
     * QuestionRepository constructor.
     *
     * @param ResourceQuestion $resource
     * @param QuestionFactory $questionFactory
     */
    public function __construct(
        ResourceQuestion $resource,
        QuestionFactory  $questionFactory
    ) {
        $this->resource = $resource;
        $this->questionFactory = $questionFactory;
    }

    /**
     * Save Question data.
     *
     * @param QuestionInterface $question
     *
     * @return QuestionInterface
     *
     * @throws CouldNotSaveException
     */
    public function save(QuestionInterface $question): QuestionInterface
    {
        try {
            /** @var \Smile\Question\Model\Question $question */
            $this->resource->save($question);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $question;
    }

    /**
     * Load Question data by given Question Identity.
     *
     * @param int $questionId
     *
     * @return QuestionInterface
     *
     * @throws NoSuchEntityException
     */
    public function getById(int $questionId): QuestionInterface
    {
        /** @var \Smile\Question\Model\Question $question */
        $question = $this->questionFactory->create();
        $this->resource->load($question, $questionId);
        if (!$question->getId()) {
            throw new NoSuchEntityException(__('Question with id "%1" does not exist.', $questionId));
        }

        return $question;
    }

    /**
     * Delete Question.
     *
     * @param QuestionInterface $question
     *
     * @return bool
     *
     * @throws CouldNotDeleteException
     */
    public function delete(QuestionInterface $question): bool
    {
        try {
            /** @var \Smile\Question\Model\Question $question */
            $this->resource->delete($question);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Question by given Question Identity.
     *
     * @param int $questionId
     *
     * @return bool
     *
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $questionId): bool
    {
        return $this->delete($this->getById($questionId));
    }
}
