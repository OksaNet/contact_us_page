<?php
/**
 * Smile Question.
 * @author    Oksana Borodina <b_oksa@ukr.net>
 * Copyright (c) 2020.
 */

declare(strict_types=1);

namespace Smile\Question\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Smile\Question\Model\Question;

/**
 * Class Status
 *
 * @package Smile\Question\Model\Source
 */
class Status implements OptionSourceInterface
{
    /**
     * Question.
     *
     * @var Question
     */
    private $question;

    /**
     * Status constructor.
     *
     * @param Question $question
     */
    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    /**
     * Get options.
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        $availableOptions = $this->question->getStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }

        return $options;
    }
}
