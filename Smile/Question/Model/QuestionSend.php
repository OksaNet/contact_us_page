<?php
/**
 * Smile Question.
 * @author    Oksana Borodina <b_oksa@ukr.net>
 * Copyright (c) 2020.
 */

declare(strict_types=1);

namespace Smile\Question\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class QuestionSend
 * @package Smile\Question\Model
 */
class QuestionSend implements QuestionSendInterface
{
    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct(
        StoreManagerInterface $storeManager,
        TransportBuilder $transportBuilder,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
    }

    public function sendEmail(array $data)
    {
        $storeId = $this->getStoreId();

        /** @var array $from */
        $from = [
            'name' => $this->scopeConfig->getValue(
                self::EMAIL_GENERAL_NAME,
                ScopeInterface::SCOPE_STORE,
                $storeId
            ),
            'email' => $this->scopeConfig->getValue(
                self::EMAIL_GENERAL_EMAIL,
                ScopeInterface::SCOPE_STORE,
                $storeId
            )
        ];

        $vars = [
            'answer' => $data['answer'],
            'name' => $data['name'],
            'store' => $this->getStore()
        ];

        $transport = $this->transportBuilder->setTemplateIdentifier(self::EMAIL_TEMPLATE)
            ->setTemplateOptions(['area' => 'frontend', 'store' => $storeId])
            ->setTemplateVars($vars)
            ->setFromByScope($from)
            ->addTo($data['email'], $data['name'])
            ->getTransport();

        $transport->sendMessage();
    }

    /**
     * get Current store id
     * @throws NoSuchEntityException
     */
    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

    /**
     * get Current store Info
     * @throws NoSuchEntityException
     */
    public function getStore()
    {
        return $this->storeManager->getStore();
    }
}
