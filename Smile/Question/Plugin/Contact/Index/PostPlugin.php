<?php
/**
 * Smile Question.
 * @author    Oksana Borodina <b_oksa@ukr.net>
 * Copyright (c) 2020.
 */

namespace Smile\Question\Plugin\Contact\Index;

use Magento\Contact\Controller\Index\Post;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Smile\Question\Api\QuestionRepositoryInterface;
use Smile\Question\Model\QuestionFactory;

/**
 * Class PostPlugin
 * @package Smile\Question\Plugin\Contact\Index
 */
class PostPlugin
{
    /**
     * @var QuestionFactory
     */
    private $question;

    /**
     * @var QuestionRepositoryInterface
     */
    private $questionRepository;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * Data persistor interface.
     *
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * PostPlugin constructor.
     * @param QuestionFactory $question
     * @param QuestionRepositoryInterface $questionRepository
     * @param DataPersistorInterface $dataPersistor
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        QuestionFactory $question,
        QuestionRepositoryInterface $questionRepository,
        DataPersistorInterface $dataPersistor,
        ManagerInterface $messageManager
    ) {
        $this->question = $question;
        $this->dataPersistor = $dataPersistor;
        $this->questionRepository = $questionRepository;
        $this->messageManager = $messageManager;
    }

    /**
     * @param Post $subject
     */
    public function beforeExecute(Post $subject)
    {
        $request = $subject->getRequest();
        try {
            /** @var \Smile\Question\Model\Question $model */
            $model = $this->question->create();

            $model->setData($this->validatedParams($subject->getRequest()));

            $this->questionRepository->save($model);

            $this->messageManager->addSuccessMessage('Saved.');

            $this->dataPersistor->clear('smile_question_save');
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $this->messageManager->addExceptionMessage($e->getPrevious() ?: $e);
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Question.'));
        }

        $this->dataPersistor->set('smile_question_save', $subject->getRequest()->getParams());
    }

    /**
     * @param $request
     * @return array
     * @throws LocalizedException
     * @throws \Exception
     */
    private function validatedParams(\Magento\Framework\App\RequestInterface $request)
    {
        if (trim($request->getParam('name')) === '') {
            throw new LocalizedException(__('Enter the Name and try again.'));
        }
        if (trim($request->getParam('comment')) === '') {
            throw new LocalizedException(__('Enter the comment and try again.'));
        }
        if (false === \strpos($request->getParam('email'), '@')) {
            throw new LocalizedException(__('The email address is invalid. Verify the email address and try again.'));
        }
        if (trim($request->getParam('hideit')) !== '') {
            throw new \Exception();
        }

        return $request->getParams();
    }
}
