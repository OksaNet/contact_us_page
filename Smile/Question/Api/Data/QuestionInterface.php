<?php
/**
 * Smile Question.
 * @author    Oksana Borodina <b_oksa@ukr.net>
 * Copyright (c) 2020.
 */

declare(strict_types=1);

namespace Smile\Question\Api\Data;

/**
 * Interface QuestionInterface
 *
 * @package Smile\Question\Api\Data
 */
interface QuestionInterface
{
    /**
     * Table name.
     */
    const TABLE_NAME = 'contact_question';

    /**#@+
     * Question's Statuses.
     */
    const STATUS_NEW      = 1;
    const STATUS_ANSWERED = 2;
    /**#@-*/

    /**#@+
     * Constants defined for keys of data array.
     */
    const ID         = 'id';
    const NAME       = 'name';
    const EMAIL      = 'email';
    const TELEPHONE  = 'telephone';
    const COMMENT   = 'question';
    const ANSWER     = 'answer';
    const STATUS     = 'status';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    /**#@-*/

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Set Name.
     *
     * @param string $name
     *
     * @return QuestionInterface
     */
    public function setName(string $name): QuestionInterface;

    /**
     * Get Email.
     *
     * @return string
     */
    public function getEmail(): string;

    /**
     * Set Email.
     *
     * @param string $email
     *
     * @return QuestionInterface
     */
    public function setEmail(string $email): QuestionInterface;

    /**
     * Get Telephone.
     *
     * @return string
     */
    public function getTelephone(): string;

    /**
     * Set Telephone.
     *
     * @param string $telephone
     *
     * @return QuestionInterface
     */
    public function setTelephone(string $telephone): QuestionInterface;

    /**
     * Get Comment.
     *
     * @return string
     */
    public function getComment(): string;

    /**
     * Set Comment
     *
     * @param string $comment
     *
     * @return QuestionInterface
     */
    public function setComment(string $comment): QuestionInterface;

    /**
     * Get Answer.
     *
     * @return string|null
     */
    public function getAnswer(): ?string;

    /**
     * Set Answer
     *
     * @param string $answer
     *
     * @return QuestionInterface
     */
    public function setAnswer(string $answer): QuestionInterface;

    /**
     * Get Status.
     *
     * @return int
     */
    public function getStatus(): int;

    /**
     * Get Statuses
     *
     * @return array
     */
    public function getStatuses(): array;

    /**
     * Set Status.
     *
     * @param int $status
     *
     * @return QuestionInterface
     */
    public function setStatus(int $status): QuestionInterface;

    /**
     * Get creation time.
     *
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Set creation time.
     *
     * @param string $creationTime
     *
     * @return QuestionInterface
     */
    public function setCreatedAt(string $creationTime): QuestionInterface;

    /**
     * Get updated time.
     *
     * @return string
     */
    public function getUpdatedAt(): string;

    /**
     * Set updated time.
     *
     * @param string $updateTime
     *
     * @return QuestionInterface
     */
    public function setUpdatedAt(string $updateTime): QuestionInterface;

}
