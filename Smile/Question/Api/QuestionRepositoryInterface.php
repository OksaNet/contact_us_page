<?php
/**
 * Smile Question.
 * @author    Oksana Borodina <b_oksa@ukr.net>
 * Copyright (c) 2020.
 */

declare(strict_types=1);

namespace Smile\Question\Api;

use Magento\Framework\Exception\NoSuchEntityException;
use Smile\Question\Api\Data\QuestionInterface;

/**
 * Interface QuestionRepositoryInterface
 *
 * @package Smile\Question\Api
 */
interface QuestionRepositoryInterface
{
    /**
     * Retrieve a question by it's id.
     *
     * @param $objectId
     *
     * @return QuestionInterface
     */
    public function getById(int $objectId): QuestionInterface;

    /**
     * Save a question.
     *
     * @param QuestionInterface $object
     *
     * @return QuestionInterface
     */
    public function save(QuestionInterface $object): QuestionInterface;

    /**
     * Delete a question by its id.
     *
     * @param int $objectId
     *
     * @return bool
     *
     * @throws NoSuchEntityException
     */
    public function deleteById(int $objectId): bool;
}
