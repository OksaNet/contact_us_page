<?php
/**
 * Smile Question.
 * @author    Oksana Borodina <b_oksa@ukr.net>
 * Copyright (c) 2020.
 */

declare(strict_types=1);

namespace Smile\Question\Ui;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Smile\Question\Model\ResourceModel\Question\CollectionFactory as CollectionFactory;

/**
 * Class QuestionDataProvider
 */
class QuestionDataProvider extends AbstractDataProvider
{
    protected $collection;

    /**
     * Loaded data.
     *
     * @var array
     */
    private $loadedData;

    public function __construct(
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
        $this->collection = $collectionFactory->create();
    }

    public function getData(): array
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }
        $items = $this->getCollection()->getItems();
        foreach ($items as $item) {
            $this->loadedData[$item->getData($this->primaryFieldName)] = $item->getData();
        }
        return $this->loadedData;
//        return $this->getCollection()->getData();
    }
}
