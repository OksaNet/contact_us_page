<?php
/**
 * Smile Question.
 * @author    Oksana Borodina <b_oksa@ukr.net>
 * Copyright (c) 2020.
 */

declare(strict_types=1);

namespace Smile\Question\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Actions
 *
 * @package Smile\Question\Ui\Component\Listing\Colum\Actions
 */
class Actions extends Column
{
    const URL_EDIT = 'smile_question/question/edit';
    const URL_DELETE = 'smile_question/question/delete';

    /**
     * Url builder.
     *
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * CommentActions construct.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct(
            $context,
            $uiComponentFactory,
            $components,
            $data
        );
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $item[$this->getData('name')] = [
                    'edit' => [
                        'href' => $this->context->getUrl(
                            static::URL_EDIT,
                            [
                                'id' => $item[$item['id_field_name']]
                            ]
                        ),
                        'label' => __('Edit')
                    ],
                    'delete' => [
                        'href' => $this->context->getUrl(
                            static::URL_DELETE,
                            [
                                'id' => $item[$item['id_field_name']]
                            ]
                        ),
                        'confirm' => [
                            'title' => __('Delete Post'),
                            'message' => __('Are you sure you want to delete a %1 record?', $item['id']),
                        ],
                        'label' => __('Delete')
                    ]
                ];
            }
        }

        return $dataSource;
    }
}
