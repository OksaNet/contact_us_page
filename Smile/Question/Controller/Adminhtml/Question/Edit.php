<?php
/**
 * Smile Question.
 * @author    Oksana Borodina <b_oksa@ukr.net>
 * Copyright (c) 2020.
 */

declare(strict_types=1);

namespace Smile\Question\Controller\Adminhtml\Question;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\View\Result\PageFactory;
use Smile\Question\Api\QuestionRepositoryInterface;

/**
 * Class Edit
 *
 * @package Smile\Question\Controller\Adminhtml\Question
 */
class Edit extends Action
{
    /**
     * Authorization level of a basic admin session.
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Smile_Question::question';

    /**
     * Page factory.
     *
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Question repository interface.
     *
     * @var QuestionRepositoryInterface
     */
    private $questionRepository;

    /**
     * Edit construct.
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param QuestionRepositoryInterface $questionRepository
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        QuestionRepositoryInterface $questionRepository
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->questionRepository = $questionRepository;
    }

    /**
     * Edit action.
     *
     * @return Page|Redirect
     */
    public function execute()
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $model = $this->questionRepository->getById((int) $id);
                $resultPage->setActiveMenu('Smile_Question::question')
                    ->getConfig()->getTitle()->prepend(__('Edit Question from %1', $model->getName()));

            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while editing the question.'));
                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        return $resultPage;
    }
}
