<?php
/**
 * Smile Question.
 * @author    Oksana Borodina <b_oksa@ukr.net>
 * Copyright (c) 2020.
 */

declare(strict_types=1);

namespace Smile\Question\Controller\Adminhtml\Question;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Exception\NoSuchEntityException;
use Smile\Question\Api\Data\QuestionInterface;
use Smile\Question\Api\QuestionRepositoryInterface;
use Smile\Question\Model\QuestionFactory;
use Smile\Question\Model\QuestionSendInterface;

/**
 * Class Save
 *
 * @package Smile\Question\Controller\Adminhtml\Question
 */
class Save extends Action
{
    /**
     * Authorization level of a basic admin session.
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Smile_Question::save';

    /**
     * Data persistor interface.
     *
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    private $questionSend;

    /**
     * Question repository interface.
     *
     * @var QuestionSendInterface
     */
    private $questionRepository;

    /**
     * Question factory.
     *
     * @var QuestionFactory
     */
    private $questionFactory;

    /**
     * Save constructor.
     *
     * @param Action\Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param QuestionRepositoryInterface $questionRepository
     * @param QuestionSendInterface $questionSend
     * @param QuestionFactory $questionFactory
     */
    public function __construct(
        Action\Context $context,
        DataPersistorInterface $dataPersistor,
        QuestionRepositoryInterface $questionRepository,
        QuestionSendInterface $questionSend,
        QuestionFactory $questionFactory
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->questionRepository = $questionRepository;
        $this->questionSend = $questionSend;
        $this->questionFactory = $questionFactory;
        parent::__construct($context);
    }

    /**
     * Save action.
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $data = $this->getRequest()->getPostValue();

        if ($data) {
            $id = $this->getRequest()->getParam('id');

            if ($id) {
                try {
                    $model = $this->questionRepository->getById((int)$id);

                    /** @var \Smile\Question\Model\Question $model */
                    $model->setData($data);
                    $model->setStatus(QuestionInterface::STATUS_ANSWERED);
                    $this->questionRepository->save($model);

                    $this->messageManager->addSuccessMessage(__('You saved the question.'));

                    if (($data['status'] == QuestionInterface::STATUS_NEW)) {
                        $this->questionSend->sendEmail($data);
                        $this->messageManager->addSuccessMessage(__('You sent the answer.'));
                    }

                    $this->dataPersistor->clear('smile_question_save');

                    return $resultRedirect->setPath('*/*/');
                } catch (NoSuchEntityException $e) {
                    $this->messageManager->addErrorMessage($e->getMessage());
                } catch (MailException $e) {
                    $this->messageManager->addExceptionMessage($e, __('Something went wrong while sending the Answer.'));
                } catch (\Exception $e) {
                    $this->messageManager->addExceptionMessage($e, __('Something went wrong while save the Question.'));
                }

                $this->dataPersistor->set('smile_question_save', $data);

                return $resultRedirect->setPath(
                    '*/*/edit',
                    ['id' => $this->getRequest()->getParam('id')]
                );
            }
        }

        return $resultRedirect->setPath('*/*/');
    }
}
